<?php

namespace RethinkGroup\SDK\Resources;

class User extends Resource
{
    public $entityName = 'users';

    public $singularEntityName = 'user';

    /**
     * Retrieve the user's access
     *
     * @param  int    $id The specified user's primary key
     *
     * @return array
     */
    public function authorize(int $id)
    {
        return $this->client->post('authorize', ['user_id' => $id])['data']['authorizations'];
    }

    /**
     * Retrieve a user by searching for their email address
     *
     * @param  string|null $email
     * @return array
     */
    public function searchByEmail(string $email)
    {
        return $this->search($email, 'email_address:like');
    }

    /**
     * Retrieve a user by searching for their email address, first name or last name
     *
     * @param  string|null $email, $first name, $last name
     *
     * @return array
     */
    public function searchByEmailFirstNameLastName(string $email = null, string $firstName = null, string $lastName = null)
    {
        $searchData = '';
        $searchFields = '';

        if ($email) {
            $searchData .= 'and|email_address:' . $email . ';';
            $searchFields .= 'email_address:like;';
        }

        if ($firstName) {
            $searchData .= 'and|first_name:' . $firstName . ';';
            $searchFields .= 'first_name:like;';
        }

        if ($lastName) {
            $searchData .= 'and|last_name:' . $lastName . ';';
            $searchFields .= 'last_name:like;';
        }

        $searchData = substr($searchData, 4);
        return $this->search($searchData, $searchFields);
    }

    /**
     * Search by multiple fields.
     *
     * @param $searchTerm
     *
     * @return array
     */
    public function omniSearch($searchTerm, $searchFirstName = null, $searchLastName = null)
    {
        // Support searching by id
        if (is_numeric($searchTerm)) {
            return $this->find($searchTerm);
        }

        return $this->searchByEmailFirstNameLastName($searchTerm, $searchFirstName, $searchLastName);
    }
}
