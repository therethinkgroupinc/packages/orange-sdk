<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class Charge
 */
class Charge extends Resource
{
    /**
     * @var string
     */
    public $entityName = 'charges';

    /**
     * @var string
     */
    public $singularEntityName = 'charge';

}
