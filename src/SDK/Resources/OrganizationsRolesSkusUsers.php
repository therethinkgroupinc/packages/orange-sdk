<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class OrganizationsRolesSkusUsers
 */
class OrganizationsRolesSkusUsers extends Resource
{
    /**
     * @var string
     */
    public $entityName = 'organizationsRolesSkusUsers';

    /**
     * @var string
     */
    public $singularEntityName = 'organizationRoleSkuUser';

    /**
     * Search for a SOUR record.
     *
     * @param  array  $fields organization_id, role_id, user_id, sku_id
     *
     * @return array
     */
    public function sourSearch($fields = [], bool $withTrashed = false)
    {
        $terms = '';
        $searchFields = '';

        $i = 0;
        foreach ($fields as $key => $value) {
            $terms .= ($i > 0) ? 'and|' : '';
            $terms .= "$key:$value;";

            $searchFields .= "$key:=;";

            $i++;
        }

        $result = $this->search($terms, $searchFields, $withTrashed);

        return !empty($result) ? $result[0] : [];
    }
}
