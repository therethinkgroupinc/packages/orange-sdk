<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class Card
 */
class Card extends Resource
{
    /**
     * @var string
     */
    public $entityName = 'cards';

    /**
     * @var string
     */
    public $singularEntityName = 'card';

    /**
     * Retrieve a card by searching for the hash id
     *
     * @param  string|null $hash
     *
     * @return array
     */
    public function searchByHash(string $hash)
    {
        return $this->search($hash, 'hash_id:like');
    }

}
