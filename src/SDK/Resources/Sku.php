<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class Sku
 */
class Sku extends Resource
{
    /**
     * @var string
     */
    protected $entityName = 'skus';

    /**
     * @var string
     */
    protected $singularEntityName = 'sku';

    /**
     * Get skus by key column.
     *
     * @param $key
     *
     * @return array
     */
    public function getByKey($key)
    {
        return $this->search($key, 'key:like');
    }
}
