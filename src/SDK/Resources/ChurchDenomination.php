<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class ChurchDenomination
 */
class ChurchDenomination extends Resource
{
    /**
     * @var string
     */
    public $entityName = 'churchDenominations';

    /**
     * @var string
     */
    public $singularEntityName = 'churchDenomination';

}
