<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class Order
 */
class Order extends Resource
{
    /**
     * @var string
     */
    protected $entityName = 'orders';

    /**
     * @var string
     */
    protected $singularEntityName = 'order';

    /**
     * Get orders by key column.
     *
     * @param $key
     *
     * @return array
     */
    public function findByUser($key)
    {
        return $this->search($key, 'user_id:=');
    }
}
