<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class Address
 */
class Address extends Resource
{
    /**
     * @var string
     */
    protected $entityName = 'addresses';

    /**
     * @var string
     */
    protected $singularEntityName = 'address';

    /**
     * Add an address to an existing organization.
     *
     * @param int $organizationId
     * @param array $data
     * @param bool $force
     *
     * @return mixed
     */
    public function storeForOrganization(int $organizationId, array $data, bool $force = false)
    {
        if ($force) {
            $data['not_validated'] = true;
        }

        return $this->client->post("organizations/$organizationId/{$this->entityName}", $data)['data'];
    }

    /**
     * {@inheritdoc}
     */
    public function update(int $id, array $data, bool $force = false)
    {
        if ($force) {
            $data['not_validated'] = true;
        }

        if (count($data) > 1) {
            return $this->client->put("{$this->entityName}/$id", $data)['data'][$this->singularEntityName];
        } else {
            return $this->client->patch("{$this->entityName}/$id", $data)['data'][$this->singularEntityName];
        }
    }

     /**
     * Retrieve a list of addresses by the updated_at column
     * @param  string $date The YYYY-MM-DD timestamp to search by
     *
     * @return array
     */
    public function getByUpdatedAt(string $date)
    {
        return $this->search($date, 'updated_at:>=', true);
    }
}
