<?php

namespace RethinkGroup\SDK\Resources;

/**
 * Class AccessControl
 */
class AccessControl extends Resource
{
    /**
     * @var string
     */
    protected $entityName = 'accessControls';

    /**
     * @var string
     */
    protected $singularEntityName = 'accessControl';

    /**
     * Search for a SOUR record.
     *
     * @param  array  $fields organization_id, user_id, sku_id
     *
     * @return array
     */
    public function accessControlSearch($fields = [], bool $withTrashed = false)
    {
        $terms = '';
        $searchFields = '';

        $i = 0;
        foreach ($fields as $key => $value) {
            $terms .= ($i > 0) ? 'and|' : '';
            $terms .= "$key:$value;";

            $searchFields .= "$key:=;";

            $i++;
        }

        $result = $this->search($terms, $searchFields, $withTrashed);

        return !empty($result) ? $result[0] : [];
    }

    /**
     * Search for accessControl rows.
     *
     * @param  array  $fields and $value
     *
     * @return array
     */
    public function accessControlSearchAll($fields = [], bool $withTrashed = false)
    {
        $terms = '';
        $searchFields = '';

        $i = 0;
        foreach ($fields as $key => $value) {
            $terms .= ($i > 0) ? 'and|' : '';
            $terms .= "$key:$value;";

            $searchFields .= "$key:=;";

            $i++;
        }

        $result = $this->search($terms, $searchFields, $withTrashed);

        return !empty($result) ? $result : [];
    }
}
